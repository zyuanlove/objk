package com.objk.admin.security.mapper;

import com.objk.admin.security.entity.SysMenu;
import com.objk.admin.security.entity.SysPermissionMenuBean;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysMenuMapper {
    /**
     * 根据ID删除
     *
     * @param menuId 主键ID
     */
    int deleteByPrimaryKey(Integer menuId);

    /**
     * 添加对象所有字段
     *
     * @param record 插入字段对象(必须含ID）
     */
    int insert(SysMenu record);

    /**
     * 根据ID查询
     *
     * @param menuId 主键ID
     */
    SysMenu selectByPrimaryKey(Integer menuId);

    /**
     * 根据ID修改对应字段
     *
     * @param record 修改字段对象(必须含ID）
     */
    int updateByPrimaryKeySelective(SysMenu record);

    /**
     * 根据ID修改所有字段(必须含ID）
     *
     * @param record 修改字段对象(必须含ID）
     */
    int updateByPrimaryKey(SysMenu record);

    /**
     * 分页查询
     */
    java.util.List<SysMenu> selectByPageNumSize();

    /**
     * 分页查询
     */
    java.util.List<SysPermissionMenuBean> selectAllPermissionCode();
}
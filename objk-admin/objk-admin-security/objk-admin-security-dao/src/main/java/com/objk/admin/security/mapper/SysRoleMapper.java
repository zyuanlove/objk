package com.objk.admin.security.mapper;

import com.objk.admin.security.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysRoleMapper {
    /**
     * 根据ID删除
     *
     * @param roleId 主键ID
     */
    int deleteByPrimaryKey(Integer roleId);

    /**
     * 添加对象所有字段
     *
     * @param record 插入字段对象(必须含ID）
     */
    int insert(SysRole record);

    /**
     * 根据ID查询
     *
     * @param roleId 主键ID
     */
    SysRole selectByPrimaryKey(Integer roleId);

    /**
     * 根据ID修改对应字段
     *
     * @param record 修改字段对象(必须含ID）
     */
    int updateByPrimaryKeySelective(SysRole record);

    /**
     * 根据ID修改所有字段(必须含ID）
     *
     * @param record 修改字段对象(必须含ID）
     */
    int updateByPrimaryKey(SysRole record);

    /**
     * 分页查询
     */
    java.util.List<SysRole> selectByPageNumSize();
}
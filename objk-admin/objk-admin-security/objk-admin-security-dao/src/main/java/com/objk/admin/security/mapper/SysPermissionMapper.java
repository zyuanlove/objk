package com.objk.admin.security.mapper;

import com.objk.admin.security.entity.SysPermission;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SysPermissionMapper {
    /**
     * 根据ID删除
     *
     * @param permissionId 主键ID
     */
    int deleteByPrimaryKey(Integer permissionId);

    /**
     * 添加对象所有字段
     *
     * @param record 插入字段对象(必须含ID）
     */
    int insert(SysPermission record);

    /**
     * 根据ID查询
     *
     * @param permissionId 主键ID
     */
    SysPermission selectByPrimaryKey(Integer permissionId);

    /**
     * 根据ID修改对应字段
     *
     * @param record 修改字段对象(必须含ID）
     */
    int updateByPrimaryKeySelective(SysPermission record);

    /**
     * 根据ID修改所有字段(必须含ID）
     *
     * @param record 修改字段对象(必须含ID）
     */
    int updateByPrimaryKey(SysPermission record);

    /**
     * 分页查询
     */
    java.util.List<SysPermission> selectByPageNumSize();

    /**
     * 分页查询
     */
    List<SysPermission> selectRolePermissionByRoleId(Integer roleId);
}
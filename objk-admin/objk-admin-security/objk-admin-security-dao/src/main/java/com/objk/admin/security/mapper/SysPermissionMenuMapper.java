package com.objk.admin.security.mapper;

import com.objk.admin.security.entity.SysPermissionMenu;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysPermissionMenuMapper {
    /**
     * 根据ID删除
     *
     * @param id 主键ID
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 添加对象所有字段
     *
     * @param record 插入字段对象(必须含ID）
     */
    int insert(SysPermissionMenu record);

    /**
     * 根据ID查询
     *
     * @param id 主键ID
     */
    SysPermissionMenu selectByPrimaryKey(Integer id);

    /**
     * 根据ID修改对应字段
     *
     * @param record 修改字段对象(必须含ID）
     */
    int updateByPrimaryKeySelective(SysPermissionMenu record);

    /**
     * 根据ID修改所有字段(必须含ID）
     *
     * @param record 修改字段对象(必须含ID）
     */
    int updateByPrimaryKey(SysPermissionMenu record);

    /**
     * 分页查询
     */
    java.util.List<SysPermissionMenu> selectByPageNumSize();
}
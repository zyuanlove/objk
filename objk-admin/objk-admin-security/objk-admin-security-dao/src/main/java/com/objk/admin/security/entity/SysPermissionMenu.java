package com.objk.admin.security.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体类 (对应表名:sys_permission_menu)
 */
public class SysPermissionMenu implements Serializable {
    /**
     * 
     */
    private Integer id;

    /**
     * 
     */
    private Integer permissionId;

    /**
     * 
     */
    private Integer menuId;

    /**
     * 权限码
     */
    private String parmissionCode;

    /**
     * 
     */
    private Date createTime;

    /**
     * 
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public String getParmissionCode() {
        return parmissionCode;
    }

    public void setParmissionCode(String parmissionCode) {
        this.parmissionCode = parmissionCode == null ? null : parmissionCode.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
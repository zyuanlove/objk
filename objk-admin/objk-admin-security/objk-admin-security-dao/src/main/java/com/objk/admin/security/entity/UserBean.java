package com.objk.admin.security.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 登陆用户表实体类 (对应表名:sys_user)
 */
@Data
public class UserBean implements Serializable {
    /**
     * 主键ID
     */
    private Integer userId;

    /**
     * 登陆用户名
     */
    private String username;

    /**
     * 登陆密码
     */
    private String password;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 操作人
     */
    private String operator;

    /**
     * 状态（0：禁用；1：可用）
     */
    private Integer status;

    /**
     * 0：未删除；1：已删除
     */
    private Integer isDelete;

    /**
     * 上次登录时间
     */
    private Date lastLoginTime;

    /**
     * 登录的ip地址
     */
    private String loginIp;
}
package com.objk.admin.security.mapper;

import com.objk.admin.security.entity.SysUserRole;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysUserRoleMapper {
    /**
     * 根据ID删除
     *
     * @param id 主键ID
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 添加对象所有字段
     *
     * @param record 插入字段对象(必须含ID）
     */
    int insert(SysUserRole record);

    /**
     * 根据ID查询
     *
     * @param id 主键ID
     */
    SysUserRole selectByPrimaryKey(Integer id);

    /**
     * 根据ID查询
     *
     * @param userId 用户id
     */
    SysUserRole selectByUserId(Integer userId);

    /**
     * 根据ID修改对应字段
     *
     * @param record 修改字段对象(必须含ID）
     */
    int updateByPrimaryKeySelective(SysUserRole record);

    /**
     * 根据ID修改所有字段(必须含ID）
     *
     * @param record 修改字段对象(必须含ID）
     */
    int updateByPrimaryKey(SysUserRole record);

    /**
     * 分页查询
     */
    java.util.List<SysUserRole> selectByPageNumSize();
}
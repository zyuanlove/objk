package com.objk.admin.security.service;

import com.objk.admin.security.entity.SysPermission;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.List;

/**
 * 用户权限接口
 *
 * @author yuan 2019/6/25 15:29
 */
public interface IPermissionService {

    List<SysPermission> getUserPermissionList(Integer userId);

    Collection<? extends GrantedAuthority> getUserAuthorities(Integer userId);

    boolean hasPermission(HttpServletRequest request, Authentication authentication);
}

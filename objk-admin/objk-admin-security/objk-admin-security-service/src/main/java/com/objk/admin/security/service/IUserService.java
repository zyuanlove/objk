package com.objk.admin.security.service;

import com.obj.commpont.common.vo.page.PageVo;
import com.objk.admin.security.entity.UserBean;
import com.objk.admin.security.param.UserPageParam;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

/**
 * 用户相关接口
 *
 * @author yuan 2019/6/19 13:21
 */
public interface IUserService extends UserDetailsService {

    /**
     * 分页获取所有用户
     *
     * @param userPageParam 分页参数
     * @return 分页用户数据
     */
    PageVo<List<UserBean>> selectByPageNumSize(UserPageParam userPageParam);

    /**
     * 获取所有用户总数
     *
     * @return 用户总数
     */
    Integer countAllUser();
}

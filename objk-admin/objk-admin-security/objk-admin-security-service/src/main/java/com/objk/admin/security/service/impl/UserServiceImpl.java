package com.objk.admin.security.service.impl;

import com.obj.commpont.common.vo.page.PageVo;
import com.objk.admin.security.entity.UserBean;
import com.objk.admin.security.mapper.UserMapper;
import com.objk.admin.security.param.UserPageParam;
import com.objk.admin.security.service.IPermissionService;
import com.objk.admin.security.service.IUserService;
import com.objk.admin.security.vo.UserVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private IPermissionService permissionService;

    @Override
    public UserDetails loadUserByUsername(String username) {
        UserBean userBean = userMapper.findByName(username);
        UserVo userVo = new UserVo();
        if (userBean == null) {
            throw new UsernameNotFoundException("用户名不存在");
        }
        BeanUtils.copyProperties(userBean, userVo);
        userVo.setEnabled(true);
        userVo.setAccountNonExpired(true);
        userVo.setAccountNonLocked(true);
        userVo.setCredentialsNonExpired(true);

        // 获取权限编码
        userVo.setAuthorities(permissionService.getUserAuthorities(userBean.getUserId()));
        return userVo;
    }

    @Override
    public PageVo<List<UserBean>> selectByPageNumSize(UserPageParam userPageParam) {
        return null;
    }

    @Override
    public Integer countAllUser() {
        return null;
    }
}

package com.objk.admin.security.constans;

import com.obj.commpont.common.constans.BaseEnum;

public enum LoginResponseCode implements BaseEnum {

    /**
     * 登录成功
     */
    LOGIN_SUCCESS(200, "登录成功"),

    LOGIN_ERROR(500, "登录失败"),

    USERNAME_NOT_FOUND(500000, "用户名不存在"),

    PASSWORD_ERROR(500001, "用户名或密码错误"),

    USER_DISENABLE(500002, "用户不可用"),

    CAPTCHA_CODE_EXPIRE_ERROR(500003, "验证码错误或过期请重新获取");

    LoginResponseCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private int code;

    private String desc;

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getDesc() {
        return desc;
    }
}

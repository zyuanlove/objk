package com.objk.admin.security.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin/user")
public class UserController {

    @RequestMapping("/list")
    public String getList(){
        return "list";
    }
}

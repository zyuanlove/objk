package com.objk.admin.permission;

import com.objk.admin.SpringJunitTest;
import com.objk.admin.security.service.IPermissionService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class PermissionServiceTest extends SpringJunitTest {

    @Autowired
    private IPermissionService permissionService;

    @Test
    public void getPermissionCode(){
        Collection<? extends GrantedAuthority> userAuthorities = permissionService.getUserAuthorities(1);

        System.out.println(userAuthorities);
    }
}

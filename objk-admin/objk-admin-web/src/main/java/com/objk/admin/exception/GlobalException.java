package com.objk.admin.exception;

import com.obj.commpont.common.vo.ServerResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * controller 全局异常处理
 *
 * @author yuan 2019/1/15 11:13
 */
@Slf4j
@ControllerAdvice
public class GlobalException {

   /* @ExceptionHandler(BindException.class)
    @ResponseBody
    public ServerResponse bindException(BindException exception) {
        List<FieldError> allErrors = exception.getBindingResult().getFieldErrors();
        String errorMsg = "参数校验异常";
        Map<String, String> errorMap = allErrors.stream().collect(
                Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
        );

        return ServerResponse.createByCodeDataMessage(400, errorMap, errorMsg);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public ServerResponse validationException(MethodArgumentNotValidException exception) {
        List<FieldError> allErrors = exception.getBindingResult().getFieldErrors();
        String errorMsg = "参数校验异常";
        Map<String, String> errorMap = allErrors.stream().collect(
                Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)
        );

        return ServerResponse.createByCodeDataMessage(400, errorMap, errorMsg);
    }*/

    @ResponseBody
    @ExceptionHandler(Exception.class)
    public ServerResponse bindException(Exception exception) {
        return ServerResponse.createByCodeDataMessage(500, "", "");
    }

}

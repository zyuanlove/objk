package com.objk.admin.controller.page;

import com.obj.commpont.common.utils.UUIDUtils;
import com.objk.admin.security.component.SecurityHolderUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PageController {

    @Autowired
    private SecurityHolderUtils securityHolderUtils;

    @RequestMapping("/admin/login.html")
    public ModelAndView loginPage() {
        ModelAndView modelAndView = new ModelAndView();
        String token = UUIDUtils.getUUID();
        modelAndView.addObject("captchaToken", token);
        modelAndView.setViewName("views/user/login");
        return modelAndView;
    }

    @RequestMapping("/404.html")
    public String error404Page() {
        return "/views/error/404";
    }

    @RequestMapping("/403.html")
    public String error403Page() {
        return "/views/error/403";
    }

    @RequestMapping("/admin/expire.html")
    public String expirePage() {
        return "/views/error/expire";
    }

    @RequestMapping("/500.html")
    public String error500Page() {
        return "/views/error/500";
    }

    @RequestMapping("/admin/index")
    public ModelAndView indexPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("currentUser", securityHolderUtils.getCurrentUser());
        modelAndView.setViewName("/views/index");
        return modelAndView;
    }

    @RequestMapping("/admin/home")
    public String homePage() {
        return "/views/home/homepage";
    }
}

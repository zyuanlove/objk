<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>后台登录页</title>
    <#include "../component/header.ftl" />
    <link rel="stylesheet" href="${basePath}/plugins/layuiadmin/style/login.css" media="all">
</head>
<body>

<div class="layadmin-user-login layadmin-user-display-show" id="LAY-user-login" style="display: none;">

    <div class="layadmin-user-login-main">
        <div class="layadmin-user-login-box layadmin-user-login-header">
            <h2>后台管理系统</h2>
            <p>znow</p>
        </div>
        <div class="layadmin-user-login-box layadmin-user-login-body layui-form">
            <div class="layui-form-item">
                <input type="hidden" name="captchaToken" id="captchaToken" value="${captchaToken}"/>
                <label class="layadmin-user-login-icon layui-icon layui-icon-username"
                       for="LAY-user-login-username"></label>
                <input type="text" name="username" lay-verify="username" id="login-username" placeholder="用户名"
                       class="layui-input">
            </div>
            <div class="layui-form-item">
                <label class="layadmin-user-login-icon layui-icon layui-icon-password"
                       for="LAY-user-login-password"></label>
                <input type="password" name="password" id="login-password"
                       placeholder="密码" lay-verify="password" class="layui-input">
            </div>
            <div class="layui-form-item">
                <div class="layui-row">
                    <div class="layui-col-xs7">
                        <label class="layadmin-user-login-icon layui-icon layui-icon-vercode"
                               for="LAY-user-login-vercode"></label>
                        <input type="text" name="captchaCode" lay-verify="captchaCode" id="captcha-code"
                               placeholder="图形验证码" class="layui-input" maxlength="4">
                    </div>
                    <div class="layui-col-xs5">
                        <div style="margin-left: 10px;">
                            <img src="" class="layadmin-user-login-codeimg"
                                 id="LAY-user-get-vercode" onclick="loadCaptchaImage()">
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item" style="margin-bottom: 20px;">
                <input type="checkbox" name="remember" lay-skin="primary" title="记住密码">
                <a href="forget.html" class="layadmin-user-jump-change layadmin-link" style="margin-top: 7px;">忘记密码？</a>
            </div>
            <div class="layui-form-item">
                <button type="button" id="login-submit" class="layui-btn layui-btn-fluid" lay-submit
                        lay-filter="LAY-user-login-submit">
                    登录
                </button>
            </div>
            <div class="layui-trans layui-form-item layadmin-user-login-other">
                <label>社交账号登入</label>
                <a href="javascript:;"><i class="layui-icon layui-icon-login-qq"></i></a>
                <a href="javascript:;"><i class="layui-icon layui-icon-login-wechat"></i></a>
                <a href="javascript:;"><i class="layui-icon layui-icon-login-weibo"></i></a>
                <a href="reg.html" class="layadmin-user-jump-change layadmin-link">注册帐号</a>
            </div>
        </div>
    </div>

    <div class="layui-trans layadmin-user-login-footer">

        <p>© 2019 <a href="" target="_blank">znow.com</a></p>
        <#--<p>
            <span><a href="" target="_blank">获取授权</a></span>
            <span><a href="" target="_blank">在线演示</a></span>
            <span><a href="" target="_blank">前往官网</a></span>
        </p>-->
    </div>

    <!--<div class="ladmin-user-login-theme">
      <script type="text/html" template>
        <ul>
          <li data-theme=""><img src="{{ layui.setter.base }}style/res/bg-none.jpg"></li>
          <li data-theme="#03152A" style="background-color: #03152A;"></li>
          <li data-theme="#2E241B" style="background-color: #2E241B;"></li>
          <li data-theme="#50314F" style="background-color: #50314F;"></li>
          <li data-theme="#344058" style="background-color: #344058;"></li>
          <li data-theme="#20222A" style="background-color: #20222A;"></li>
        </ul>
      </script>
    </div>-->

</div>
<#include "../component/footer.ftl" />
<script type="text/javascript">
     $(function(){
         login.initPage();
     });
     var login = {
         //初始化页面跳转，为了防止从iframe跳转到login页面直接在iframe中显示login页面
         initPage : function() {
             if(window.top != window.self){
                 alert(location.href);
                 top.location.href = location.href;
             }
         },
     }
</script>
<script type="text/javascript">
    layui.config({
        base: '${basePath}/plugins/layuiadmin/' //静态资源所在路径
    }).use(['form'], function () {
        var form = layui.form;
        loadCaptchaImage();
        form.verify({
            username: function (value) {
                if (isEmpty(value)) {
                    return "用户名不能为空！";
                }
            },
            password: function (value) {
                if (isEmpty(value)) {
                    return "密码不能为空！";
                }
            },
            captchaCode: function (value) {
                if (isEmpty(value)) {
                    return "验证码不能为空！";
                }
            }
        });

        form.on("submit(LAY-user-login-submit)", function (obj) {
            var loginSubmit = $("#login-submit");
            // load层
            var loadIndex = layer.load(1, {
                shade: [0.1, '#000'] //0.1透明度的白色背景
            });
            $.ajax({
                url: "${basePath}/admin/login",
                type: "POST",
                data: obj.field,
                success: function (data) {
                    var data = data;
                    if (data && data.code == 200) {
                        layer.msg("登录成功", {
                            offset: '20px',
                            icon: 2
                        });
                        alert(JSON.stringify(data))
                        window.location.href = "${basePath}/admin/index";
                    } else {
                        errorMsg(data.msg);
                    }
                },
                beforeSend: function () {
                    loginSubmit.attr("disabled", true).html("正在登录...");
                },
                complete: function () {
                    layer.close(loadIndex);
                    loginSubmit.removeAttr("disabled").html("登录");
                }
            })
            return false;
        })
    });

    function loadCaptchaImage() {
        var uuid = $("#captchaToken").val();
        if (uuid) {
            var imageUrl = "${basePath}/captcha/code/" + uuid + "?t=" + new Date().getTime();
            $("#LAY-user-get-vercode").attr("src", imageUrl);
        }
        return false;
    }

    function errorMsg(msg) {
        if (msg) {
            //实际使用时记得删除该代码
            layer.msg(msg, {
                offset: '20px',
                icon: 2,
                time: 5000
            });
        }

    }

</script>
</body>
</html>
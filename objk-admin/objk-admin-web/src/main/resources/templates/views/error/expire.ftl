<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>404 页面不存在</title>
    <#include "../component/header.ftl" />
</head>
<body>
<div class="layui-fluid">
    <div class="layadmin-tips">
        <i class="layui-icon" face>&#xe664;</i>
        <div class="layui-text">
            <h1>
                <span class="layui-anim layui-anim-loop layui-anim-">用户</span>
                <span class="layui-anim layui-anim-loop layui-anim-rotate">会话</span>
                <span class="layui-anim layui-anim-loop layui-anim-">过期</span>
            </h1>
        </div>
    </div>
</div>
</body>
</html>
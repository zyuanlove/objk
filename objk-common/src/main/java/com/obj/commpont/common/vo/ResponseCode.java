package com.obj.commpont.common.vo;

import com.obj.commpont.common.constans.BaseEnum;

/**
 * TODO
 *
 * @author yuan 2018/10/11 15:07
 */
public enum ResponseCode implements BaseEnum {

    /**
     * 返回成功消息
     **/
    SUCCESS(200, "SUCCESS"),

    /**
     * 返回错误消息
     **/
    ERROR(400, "ERROR");

    private final int code;

    private final String desc;

    ResponseCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getName() {
        return null;
    }

}

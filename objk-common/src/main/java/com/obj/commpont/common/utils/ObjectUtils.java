package com.obj.commpont.common.utils;

import org.springframework.beans.BeanUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * TODO
 *
 * @author yuan 2018/12/4 17:30
 */
public class ObjectUtils {

    public static boolean checkFieldValueNull(Object target) {
        boolean flag = true;
        if (target != null) {
            Class<?> actualEditable = target.getClass();
            PropertyDescriptor[] targetPds = BeanUtils.getPropertyDescriptors(actualEditable);
            for (PropertyDescriptor targetPd : targetPds) {
                Method readMethod = targetPd.getReadMethod();
                //如果是私有的方法可以访问
                readMethod.setAccessible(true);
                //如果属性是对象类型
                if (targetPd.getPropertyType() instanceof Object) {

                    //过滤class属性
                    if (!"class".equals(targetPd.getName())) {
                        //获取属性对应的值
                        try {
                            Object value = readMethod.invoke(target);
                            if (value != null) {
                                flag = false;
                                break;
                            }
                        } catch (IllegalAccessException e) {
                            //logger.error("checkFieldValueNull:{}", e);
                        } catch (InvocationTargetException e) {
                            //logger.error("checkFieldValueNull:{}", e);
                        }
                    }
                }
            }
        }
        return flag;
    }
}

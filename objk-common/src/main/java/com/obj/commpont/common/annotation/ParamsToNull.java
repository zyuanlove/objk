package com.obj.commpont.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 把方法中传入的指定参数设置为空
 *
 * @author dell 2018/11/16 11:30
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ParamsToNull {

    String[] value() default {"createTime","modifyTime"};

}

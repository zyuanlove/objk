package com.obj.commpont.common.utils;


import org.apache.commons.lang3.RandomStringUtils;

/**
 * 生成数据数工具类依赖于{@link RandomStringUtils}
 *
 * @author dell 2018/4/28 15:23
 */
public class RandomUtils {

    /**
     * 验证码随机数去掉容易混淆i,o,0,1
     **/
    public static final char[] CAPTCHA_CODE = "123456789abcdefghjklmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ".toCharArray();

    /**
     * 数字和字母随机数
     **/
    public static final char[] RANDOM_CODE = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

    /**
     * 纯数字随机数
     **/
    public static final char[] NUMBER_CODE = "0123456789".toCharArray();

    /**
     * 生成一个指定长度和指定字符范围的随机字符串
     *
     * @param length 指定长度
     * @param chars  指定字符范围
     * @return 随机数
     **/
    public static String getRandom(int length, char[] chars) {
        return RandomStringUtils.random(length, chars);
    }

    /**
     * 生成一个指定长度的随机字符串
     *
     * @param length 指定长度
     * @return 随机数
     **/
    public static String getRandom(int length) {
        return RandomStringUtils.random(length, RANDOM_CODE);
    }

    /**
     * 生成一个指定长度的纯数字随机字符串
     *
     * @param length 指定长度
     * @return 随机数
     **/
    public static String getRandomNumber(int length) {
        return RandomStringUtils.random(length, NUMBER_CODE);
    }
}

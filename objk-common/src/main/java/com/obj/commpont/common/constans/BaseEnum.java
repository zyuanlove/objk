package com.obj.commpont.common.constans;

/**
 * 枚举通用接口
 * @author yuan 2018/7/25 9:22
 */
public interface BaseEnum {

    int getCode();

    String getName();

    String getDesc();

}

package com.obj.commpont.common.vo.page;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * PageInfo的包装类，便于在API中显示
 *
 * @author yuan 2018/4/9 17:37
 */
@ApiModel(description = "分页结果集,包含分页需要的相关数据")
public class PageVo<T> extends PageInfo<T> {

    @ApiModelProperty("当前页")
    private int pageNum;

    @ApiModelProperty("每页显示数量")
    private int pageSize;

    @ApiModelProperty("当前页数量")
    private int size;

    @ApiModelProperty("当前页面第一个元素在数据库中的行号")
    private int startRow;

    @ApiModelProperty("当前页面最后一个元素在数据库中的行号")
    private int endRow;

    @ApiModelProperty("总记录数")
    private long total;

    @ApiModelProperty("总页数")
    private int pages;

    @ApiModelProperty("分页查询结果集")
    private List<T> list;

    @ApiModelProperty("上一页码号")
    private int prePage;

    @ApiModelProperty("下一页码号")
    private int nextPage;

    @ApiModelProperty("是否为第一页")
    private boolean isFirstPage = false;

    @ApiModelProperty("是否为最后一页")
    private boolean isLastPage = false;

    @ApiModelProperty("是否有前一页")
    private boolean hasPreviousPage = false;

    @ApiModelProperty("是否有下一页")
    private boolean hasNextPage = false;

    @ApiModelProperty("第一页页码")
    private int firstPage;

    @ApiModelProperty("最后一页页码")
    private int lastPage;

    @ApiModelProperty("导航页码数")
    private int navigatePages;

    @ApiModelProperty("所有导航页号")
    private int[] navigatepageNums;

    @ApiModelProperty("导航条上的第一页")
    private int navigateFirstPage;

    @ApiModelProperty("导航条上的最后一页")
    private int navigateLastPage;

    public PageVo() {
    }

    public PageVo(List<T> list) {
        super(list);
    }
}

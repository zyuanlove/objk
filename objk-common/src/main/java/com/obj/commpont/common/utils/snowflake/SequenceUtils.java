package com.obj.commpont.common.utils.snowflake;


/**
 * 基于Twitter的Snowflake算法实现分布式高效有序ID生产黑科技(sequence)
 */
public class SequenceUtils {

    private static long workerId = 0L;

    private static long dataCenterId = 0L;

    private static Sequence sequence = new Sequence(workerId, dataCenterId);

    public static Long nextLongId() {
        return sequence.nextId();
    }

    public static String nextStringId() {
        return String.valueOf(sequence.nextId());
    }

}
package com.obj.commpont.common.param;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Range;

/**
 * TODO
 *
 * @author yuan 2019/1/8 19:50
 */
public class PageParam {

    @ApiModelProperty(value = "分页大小")
    @Range(min = 0,max = Integer.MAX_VALUE,message = "分页大小超出范围")
    private Integer pageNum = 1;

    @ApiModelProperty(value = "分页数量")
    @Range(min = 0,max = Integer.MAX_VALUE,message = "分页数量超出范围")
    private Integer pageSize = 10;

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public Integer getPageNum() {
        return pageNum;
    }
}

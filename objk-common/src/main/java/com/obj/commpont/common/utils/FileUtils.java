package com.obj.commpont.common.utils;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 功能说明：<br/>
 * 文件路径与文件信息获取工具类
 * </br>
 * 开发人员：pengzz(pengzz@strongit.com.cn)<br/>
 * 开发时间：2015年4月13日<br/>
 */
public class FileUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);

    private FileUtils() {

    }

    /**
     * 获取文件的后缀名
     * @param file 文件对象
     * @return 文件不存在或文件是目录时返回空字符串
     */
    public static String getFileExtension(File file) {
        String fileExtension = "";
        // 文件必须存在且不是目录
        if (null != file && file.exists() && !file.isDirectory()) {
            fileExtension = getFilextension(file.getName());
        }
        return fileExtension;
    }

    /**
     * 根据文件名获取文件的后缀
     * @param fileName 文件名
     * @return 如果参数为空将返回空字符串
     */
    public static String getFilextension(String fileName) {
        String fileExtension = "";

        if (StringUtils.isNotBlank(fileName)) {
            fileExtension = StringUtils.substring(fileName, fileName.lastIndexOf(".") + 1, fileName.length());
        }

        return fileExtension;
    }

    /**
     * 获取classes的根路径
     * <li>应用容器中根路径为：{web}/WEB-INF/classes/
     * <li>测试环境中根路径为：{project}/target/test-classes/
     * @return classes加载路径的根路径(绝对路径，包含文件分隔符)
     */
    public static String getClassRootPath() {
        String absolutePath = FileUtils.class.getClassLoader().getResource("").getPath();
        return decode(absolutePath);
    }

    /**
     * 获取项目的根路径
     * <li>应用容器中返回值为：{web path}
     * <li>测试环境中根路径为：{project path}
     * @return 项目根路径(不以文件分隔符结束)
     */
    public static String getProjectRootPath() {
        File classRootPath = new File(getClassRootPath());
        return classRootPath.getParentFile().getParentFile().getAbsolutePath();
    }

    /**
     * 获取clasz的绝对路径
     * @param clasz
     * @return
     */
    public static String getClassPath(Class<?> clasz) {
        String classAbsolutePath = clasz.getResource("").getPath();
        return decode(classAbsolutePath);
    }

    /**
     * 文件路径中包括特殊字符时将导致路径访问失败，需要调用此方法进行路径解码
     * @param filePath 文件路径
     * @return 解码后的路径
     */
    public static String decode(String filePath) {
        try {
            filePath = URLDecoder.decode(filePath, "utf-8");
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("对{}进行解码失败", e, filePath);
        }
        return filePath;
    }

    /**
     * 检测文件夹路径下文件是否存在,若不存在则创建文件夹
     * @param path 文件夹路径
     * @return true 文件夹路径存在或创建成功
     */
    public static boolean checkAndCreateFolder(String path) {
        File file = new File(path);
        return checkAndCreateFolder(file);
    }

    /**
     * 检测文件夹是否存在,若不存在则创建文件夹
     * @param file 文件夹对象
     * @return true 文件夹路径存在或创建成功
     */
    public static boolean checkAndCreateFolder(File file) {
        boolean flag = true;
        if (!file.exists()) {
            flag = file.mkdirs();
        }
        return flag;
    }

    /**
     * 检查文件路径下文件是否存在，若不存在创建该文件
     * @param path 文件路径
     * @return true 文件路径下的文件存在或创建成功
     * @throws IOException
     */
    public static boolean checkAndCreateFile(String path) throws IOException {
        File file = new File(path);
        return checkAndCreateFile(file);
    }

    /**
     * 检查文件是否存在，若不存在则创建该文件
     * @param file 文件对象
     * @return true 文件存在或创建成功
     * @throws IOException
     */
    public static boolean checkAndCreateFile(File file) throws IOException {
        boolean flag = true;
        if (!file.exists()) {
            File folder = file.getParentFile();
            if (!folder.exists()) {
                flag = folder.mkdirs();
            }
            if (flag) {
                flag = file.createNewFile();
            }
        }
        return flag;
    }

    public static String getMD5(File file){
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(org.apache.commons.io.FileUtils.readFileToByteArray(file));
            BigInteger bigInt = new BigInteger(1, md5.digest());
            return bigInt.toString(16);
        } catch (NoSuchAlgorithmException | IOException e) {
            LOGGER.error(e.getMessage(),e);
        }
        return StringUtils.EMPTY;
    }
}
